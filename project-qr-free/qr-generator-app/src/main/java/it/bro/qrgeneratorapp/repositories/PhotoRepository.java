package it.bro.qrgeneratorapp.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import it.bro.qrgeneratorapp.models.Photo;

@Repository
public interface PhotoRepository extends MongoRepository< Photo , String> {
    
}