package it.bro.qrgeneratorapp.controllers;

import java.io.IOException;
import java.util.Base64;

import com.google.zxing.WriterException;

import it.bro.qrgeneratorapp.models.Photo;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import it.bro.qrgeneratorapp.services.PhotoServiceImpl;
import it.bro.qrgeneratorapp.services.QRCodeGeneratorImpl;

@RestController
public class PhotoController {

    Logger logger = LoggerFactory.getLogger(PhotoController.class);

    @Autowired
    private PhotoServiceImpl photoService;

    @Autowired
    private QRCodeGeneratorImpl qrgen;

    @PostMapping("/photos/add")
    public Photo addPhoto(@RequestParam("title") String title, @RequestParam("image") MultipartFile image)
            throws IOException {
        String id = photoService.addPhoto(title, image);
        Photo photo = new Photo();
        photo.setId(id);
        return photo;
    }

    @GetMapping("/photos/{id}")
    public Photo getPhoto(@PathVariable String id) {
        Photo photo = photoService.getPhoto(id);
        return photo;
    }

    @GetMapping("/photos/qr/{id}")
    public Photo getPhotoQR(@PathVariable String id) throws WriterException, IOException {
        Photo photo = photoService.getPhoto(id);
        photo.setImage(new Binary(qrgen.getQRCodeImage(photo.getId(), 300, 300)));
        return photo;
    }
}