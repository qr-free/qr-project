package it.bro.qrgeneratorapp.controllers;

import java.io.IOException;
import java.util.Base64;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import it.bro.qrgeneratorapp.models.Photo;
import it.bro.qrgeneratorapp.services.PhotoServiceImpl;
import it.bro.qrgeneratorapp.services.QRCodeGeneratorImpl;

@RestController
@CrossOrigin(origins = "http://167.172.36.108:80")
public class PhotoController {

    @Autowired
    private PhotoServiceImpl photoService; 

    @Autowired
    private QRCodeGeneratorImpl qrgen;

    @PostMapping("/photos/add")
    public Photo addPhoto(@RequestParam("title") String title, @RequestParam("image") MultipartFile image)
            throws IOException {
        String id = photoService.addPhoto(title, image);
        return "photo id --> " + id;
    }

    @GetMapping("/photos/{id}")
    public Photo getPhoto(@PathVariable String id) {
        Photo photo = photoService.getPhoto(id);
        return photo;
    }
}